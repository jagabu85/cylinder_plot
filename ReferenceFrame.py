import numpy as np
from scipy.linalg import norm

class ReferenceFrame():
    """
    Given two points (point_0 and point_1) this class defines an orthogonal
    reference frame (n1, n2 and n3) where the first axis starts at point_0 and
    points towards point_1
    """
    def __init__(self, point_0, point_1):

        # Frame [n1, n2, n3] - Orthogonal coordinate system
        self.n1 = None # Axis aligned with the vector from point_0 to point_1
        self.n2 = None
        self.n3 = None

        self.set_reference_frame(point_0, point_1)

    def print_info(self):
        print("n1: ", self.n1)
        print("n1: ", self.n2)
        print("n1: ", self.n3)


    def get_unitary_vector_between_points(self, p_0, p_1):
        v = p_1 - p_0
        return v / norm(v)

    def set_reference_frame(self, point_0, point_1):
        self.n1 = self.get_unitary_vector_between_points(point_0, point_1)
        not_v = np.array([0, 0, -1])
        if (self.n1 == not_v).all():
            not_v = np.array([0, 1, 0])

        self.n2 = np.cross(self.n1, not_v)  # n2 perpendicular to n1
        self.n2 /= norm(self.n2)  # normalize n2
        self.n3 = np.cross(self.n1, self.n2)  # n3 unit vector perpendicular