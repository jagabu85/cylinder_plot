# -*- coding: utf-8 -*-
"""
Created on Mon May 30 17:59:53 2022

@author: mr7117
"""
"""
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
##arrayLucc is the array of land use types 
arrayLucc = np.random.randint(1,3,(5,5))
## first you need to define your color map and value name as a dic
t = 1 ## alpha value
cmap = {1:[0.1,0.1,1.0,t],2:[1.0,0.1,0.1,t]}
labels = {1:'agricultural land',2:'forest land'}
arrayShow = np.array([[cmap[i] for i in j] for j in arrayLucc])    
## create patches as legend
patches =[mpatches.Patch(color=cmap[i],label=labels[i]) for i in cmap]

plt.imshow(arrayShow)
plt.legend(handles=patches, loc=2, borderaxespad=0.)
plt.show()
"""

import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
colors = ["g", "b"]
texts = ["Liquid", "Solid"]
patches = [ plt.plot([],[], marker="s", ms=10, ls="", mec=None, color=colors[i], 
            label="{:s}".format(texts[i]) )[0]  for i in range(len(texts)) ]
plt.legend(handles=patches, bbox_to_anchor=(1, 1), ncol=4)

plt.show()