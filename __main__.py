#!/usr/bin/env basic
# -*- coding: utf-8 -*-

"""
Created on Wed Nov 11 10:30:00 2021
@author: jagabu85
"""

"""
Abreviations:
 num: number of
 variable_discret: discretization, means that variable it is a vector from 0 to
  the value of the variable
"""

import numpy as np
from utils import *
from matplotlib import pyplot as plt
from matplotlib import cm
from SemiCylinder import SemiCylinder
from input_values import *

def create_semicylinder_geometries():
    """
    :return: List of SemiCylinder objects
    """
    # General characteristiques
    info_a = get_a_quarter_info()
    info_b = get_b_quarter_info()
    info_c = get_c_quarter_info()
    info_d = get_d_quarter_info()

    geometries = []

    for info in [info_a, info_b, info_c, info_d]:
        geometries.append(SemiCylinder(info["point_0"], info["point_1"],
                                       info["slice_thickness"],
                                       info["radius"], info["angle"],
                                       info["angle_start"],
                                       info["slices_discretization"]))

    return geometries

def plot_values(geometries, values, colorbar_label):
    """
    :param geometries: List of SemiCylinder objects
    :param values: List of np.arrays with the values of each geometry
    :param colorbar_label: String with the label for the colorbar
    :return:
    """
    set_values_into_semicylinders(geometries, values)
    
    fig = plt.figure()
    ax = plt.subplot(111, projection='3d')
    color_map = plot_colorbar(values, colorbar_label, ax)
    # Plot cylinder
    for geometry in geometries:
        geometry.plot(ax, color_map)

    set_plot_configuration(ax)
    return ax

def set_values_into_semicylinders(geometries, values):
    """
    :param geometries: List of SemiCylinder objects
    :param values: List of np.arrays with the values of each geometry
    :return:
    """

    if len(values) != len(geometries):
        raise Exception("Values and geometries should be the same dimension. "
                        "Length of values = " , len(values), " Length of "
                        "geometries = ", len(geometries))

    for i, geometry in enumerate(geometries):
        geometry.set_values(values[i])

    return

def plot_colorbar(values, colorbar_label, ax):
    """
    :param values: List of np.arrays with the values of each geometry
    :param colorbar_label: String with the label for the colorbar
    :return:
    """
    #Colorbar
    color_map = cm.ScalarMappable(cmap=cm.rainbow)
    color_map.set_array(get_cbar_limits(values))
    cbar = plt.colorbar(color_map, ax=ax, shrink=0.5)
    cbar.ax.set_title(colorbar_label)
    cbar.ax.tick_params(labelsize=10)

    return color_map

def get_cbar_limits(values):

    if get_user_limits() != None:
        cbar_limits = get_user_limits()
        return cbar_limits
    else:
        return values

def set_plot_configuration(ax):
    # Set axis labels and grid configuration
    axisEqual3D(ax)
    plt.grid(False)
    plt.axis('off')

    #set perspective
    ax.view_init(28, -60)

def plot_average_halfs(measurements, label):
    value_ab = np.mean([measurements[0], measurements[1]], axis=0)
    value_cd = np.mean([measurements[2], measurements[3]], axis=0)
    ax = plot_values(geometries, [value_ab, value_ab, value_cd, value_cd],
                     label)
    return ax

def plot_slice_average(measurements, label):
    value_average = np.mean(measurements, axis=0)
    ax = plot_values(geometries, [value_average, value_average, value_average,
                                  value_average], label)
    return ax

def save_plot(label, path = None):
    if path == None:
        plt.savefig(label + '.png', dpi = 600)
        print(label, " plot saved! In working folder")
    else:
        plt.savefig(path + label + '.png', dpi=600)
        print(label, " plot saved ! In path.")

if __name__ == "__main__":
    measurements = get_measuremnts()
    geometries = create_semicylinder_geometries()

    plot_values(geometries, measurements, get_title())
    save_plot(get_title() + '_quarters', get_path_to_save_plot())
    plot_average_halfs(measurements, get_title())
    save_plot(get_title() + '_average_halfs', get_path_to_save_plot())
    plot_slice_average(measurements, get_title())
    save_plot(get_title() + '_slice_average', get_path_to_save_plot())

    plt.show()