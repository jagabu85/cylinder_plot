import numpy as np

class SemiCylinderSlice:
    """ This class creates the necessary points to plot a quarter of cylinder.
    It also makes the plots and the surface colour is defined by a value
    coming from outside the class.
    angle_division is the discretization used to modify the resolution
    radius_division and length_division are not used yet, but are defined
    for a future feature
    """
    def __init__(self, point_0, frame, thickness, radius, angle,
                 angle_start, discretization):
        """Constructor method
        """
        self.point_0 = point_0
        self.frame = frame
        self.radius = radius
        self.angle = angle
        self.angle_start = angle_start
        self.thickness = thickness

        self.length_divisions = discretization["length"]
        self.radius_divisions = discretization["radius"]
        self.angle_divisions = discretization["angle"]



    def print_info(self):
        """Print the variables stored in the Class CheeseSlice
        """
        print('point_0:', self.point_0)
        print('frame:', self.frame)
        print('thickness:', self.thickness)
        print('radius:', self.radius)
        print('angle:', self.angle)
        print('length_divisions:', self.length_divisions)
        print('radius_divisions:', self.radius_divisions)
        print('angle_divisions:', self.angle_divisions)

    def calculate_tube_grid_points(self):
        thickness_discret = np.linspace(0, self.thickness,
                                     self.length_divisions)
        angle_discret = np.linspace(self.angle_start,
                                    self.angle_start + self.angle,
                                    self.angle_divisions)
        _, angle_grid = np.meshgrid(thickness_discret, angle_discret)

        x, y, z = [self.point_0[i] + self.frame.n1[i] * thickness_discret +
                   self.radius * np.cos(angle_grid) *
                   self.frame.n2[i] +
                   self.radius * np.sin(angle_grid) *
                   self.frame.n3[i]
                   for i in [0, 1, 2]]
        return x, y, z

    def calculate_bottom_base_grid_points(self):
        radius_discret = np.linspace(0, self.radius, self.radius_divisions)
        angle_discret = np.linspace(self.angle_start,
                                    self.angle_start + self.angle,
                                    self.angle_divisions)
        _, angle_grid = np.meshgrid(radius_discret, angle_discret)

        x, y, z = [self.point_0[i] +
                radius_discret * np.cos(angle_grid) * self.frame.n2[i] +
                radius_discret * np.sin(angle_grid) * self.frame.n3[i]
                for i in [0, 1, 2]]
        return x, y, z

    def calculate_top_base_grid_points(self):
        radius_discret = np.linspace(0, self.radius, self.radius_divisions)
        angle_discret = np.linspace(self.angle_start,
                                    self.angle_start + self.angle,
                                    self.angle_divisions)
        _, angle_grid = np.meshgrid(radius_discret, angle_discret)

        x, y, z = [self.point_0[i] + self.frame.n1[i] * self.thickness +
                   radius_discret * np.cos(angle_grid) * self.frame.n2[i] +
                   radius_discret * np.sin(angle_grid) * self.frame.n3[i]
                   for i in [0, 1, 2]]
        return x, y, z

    def calculate_left_side_grid_points(self):
        thickness_discret = np.linspace(0, self.thickness,
                                     self.length_divisions)

        radius_discret = np.linspace(0, self.radius, self.radius_divisions)
        _, radius_grid = np.meshgrid(thickness_discret, radius_discret)

        x, y, z = [self.point_0[i] +
                   radius_grid * np.cos(self.angle_start) *
                   self.frame.n2[i] +
                   + radius_grid * np.sin(self.angle_start) *
                   self.frame.n3[i] + self.frame.n1[i] * thickness_discret
                   for i in [0, 1, 2]]

        return x, y, z

    def calculate_right_side_grid_points(self):
        thickness_discret = np.linspace(0, self.thickness,
                                     self.length_divisions)

        radius_discret = np.linspace(0, self.radius, self.radius_divisions)
        _, radius_grid = np.meshgrid(thickness_discret, radius_discret)

        x, y, z = [self.point_0[i] +
                   radius_grid * np.cos(self.angle_start + self.angle) *
                   self.frame.n2[i] +
                   + radius_grid * np.sin(self.angle_start + self.angle) *
                   self.frame.n3[i] + self.frame.n1[i] * thickness_discret
                   for i in [0, 1, 2]]

        return x, y, z

    def calculate_lateral_grid_point(self):
        x1, y1, z1 = self.calculate_left_side_grid_points()
        x2, y2, z2 = self.calculate_right_side_grid_points()
        x = np.concatenate((x1, x2))
        y = np.concatenate((y1, y2))
        z = np.concatenate((z1, z2))

        return x, y, z

    def plot(self, ax, value_color):
        """
        :param ax:
        :param value_color: This value comes from outside the class
        :return:
        """

        x1, y1, z1 = self.calculate_tube_grid_points()
        x2, y2, z2 = self.calculate_bottom_base_grid_points()
        x3, y3, z3 = self.calculate_top_base_grid_points()
        x4, y4, z4 = self.calculate_lateral_grid_point()

        ax.plot_surface(x1, y1, z1, color=value_color, alpha=1, linewidth=10,
                        antialiased=True,shade=False)
        ax.plot_surface(x2, y2, z2, color=value_color, alpha=1, linewidth=10,
                        antialiased=True,shade=False)
        ax.plot_surface(x3, y3, z3, color=value_color, alpha=1, linewidth=10,
                        antialiased=True,shade=False)
        ax.plot_surface(x4, y4, z4, color=value_color, alpha=1, linewidth=10,
                        antialiased=True,shade=False)