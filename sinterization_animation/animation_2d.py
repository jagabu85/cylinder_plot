import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import time
from celluloid import Camera

data = { 
        'good rod' : np.array([[1204.66298,1209.355175,1201.627579,1205.260573,1207.034377,1201.011011,1201.011011,1202.851226,1204.66298,1204.062224,1201.011011,1201.011011,1199.768389,1203.458307,1201.627579,1199.768389,1201.627579,1202.240984,1204.66298,1201.627579,1203.458307,1202.851226,1202.851226,1201.627579,1202.240984,1201.627579,1202.851226,1199.142334,1199.142334,1199.768389,1201.011011,1202.851226,1204.66298],
                      [1208.779719,1213.294818,1211.062568,1209.927468,1203.458307,1207.61932,1204.66298,1204.062224,1204.062224,1204.66298,1203.458307,1205.855003,1205.855003,1204.062224,1204.66298,1204.062224,1198.513117,1201.627579,1205.855003,1203.458307,1204.66298,1205.855003,1210.496599,1206.446271,1204.66298,1204.66298,1201.627579,1202.240984,1200.391281,1204.66298,1207.034377,1208.201101,1208.779719]]),
        'bent rod' : np.array([[1195.964623,1197.880737,1194.671402,1194.671402,1194.020047,1192.707851,1194.671402,1193.36553,1193.36553,1194.020047,1194.020047,1193.36553,1193.36553,1192.047009,1193.36553,1194.020047,1193.36553,1192.707851,1193.36553,1192.707851,1192.707851,1192.707851,1194.020047,1194.020047,1194.020047,1194.671402,1193.36553,1194.671402,1194.020047,1194.671402,1192.047009,1194.020047,1194.671402],
                      [1194.671402,1194.671402,1192.047009,1191.383005,1190.715839,1190.715839,1190.715839,1190.715839,1190.04551,1190.04551,1188.695365,1188.015548,1188.015548,1188.015548,1188.015548,1188.015548,1188.695365,1188.015548,1188.695365,1189.372018,1189.372018,1189.372018,1188.695365,1188.015548,1189.372018,1188.695365,1188.695365,1190.04551,1189.372018,1190.04551,1189.372018,1190.715839,1190.04551]]),
        '3 times straighten' : np.array([[1196.60649,1195.319593,1194.671402,1195.319593,1194.671402,1195.964623,1195.964623,1195.964623,1195.964623,1195.319593,1195.964623,1196.60649,1197.245195,1197.245195,1196.60649,1197.880737,1197.245195,1197.245195,1197.245195,1197.245195,1197.880737,1197.245195,1197.245195,1197.245195,1197.880737,1197.245195,1199.768389,1196.60649,1197.245195,1196.60649,1199.142334,1198.513117,1205.260573],
                      [1196.60649,1194.671402,1192.047009,1191.383005,1192.707851,1192.047009,1194.020047,1192.707851,1193.36553,1193.36553,1194.020047,1194.020047,1194.671402,1194.671402,1194.671402,1195.964623,1195.319593,1194.671402,1194.020047,1195.964623,1196.60649,1195.319593,1196.60649,1197.245195,1194.020047,1195.964623,1195.964623,1195.319593,1197.245195,1195.319593,1195.964623,1199.142334,1202.240984]])
        }

def get_min_max(data):
    min_temp = float('inf')
    max_temp = float('-inf')
    
    for key, data in data.items():
        if max_temp < data.max():
            max_temp = data.max()
        if min_temp > data.min():
            min_temp = data.min()
    return min_temp, max_temp

def get_temperature_discretization(data):
    min_temperature, max_temperature = get_min_max(data)
    temperatur_discret = np.linspace(max_temperature, min_temperature, 100)

    return temperatur_discret

def get_cooling_ratio():
     # 6/10 [ºC/min] = 100ºC/h
     return 6/10
 
def get_time_discretization(temperatur_discret):
    time_discret = np.zeros(len(temperatur_discret))
    time_discret[1:] = ((temperatur_discret[:-1] - temperatur_discret[1:]) * 
                        get_cooling_ratio())
    time_discret = np.cumsum(time_discret)
    
    return time_discret

def get_solid_liquid_patch():
    colors = ["b", "r"]
    texts = ["Liquid", "Solid"]
    patches = [plt.plot([],[], marker="s", ms=10, ls="", mec=None, 
               color=colors[i], label="{:s}".format(texts[i]) )[0]  
               for i in range(len(texts)) ]
    return patches

def get_temperature_range(data):
    temperature_range = []
    for key, bend_road in data.items():
        temperature_range.append(bend_road.max() - bend_road.min())
    print(temperature_range)

    return temperature_range

def create_comparison_animation_solidification(data):
    temperatur_discret = get_temperature_discretization(data)
    time_discret = get_time_discretization(temperatur_discret)

    temperature_range = get_temperature_range(data)
    
    fig, ax = plt.subplots(1, 3, figsize=(20, 5), sharey=True)
    camera = Camera(fig)        
    tmp_temperature = 0
    patches = get_solid_liquid_patch()
    
    for i, actual_temperature in enumerate(temperatur_discret):
        for j, key in enumerate(data):
            ax[j].imshow(data[key] > actual_temperature, cmap='bwr', vmin=0,
                         vmax=1)
            ax[j].title.set_text(key)
            
            ax[j].text(1, 5, "Range temperature = " + 
                       str(round(temperature_range[j],2)) + "ºC")
            ax[j].text(1, 6.2, "Solidification time = " + 
                       str(round(temperature_range[j]*get_cooling_ratio(),2)) + 
                       "min")        
            
        ax[1].text(1, 9, "Temperature T = " + str(round(actual_temperature, 
                                                        2)) + "ºC")
        ax[1].text(1, 10, "Time t = " + str(round(time_discret[i], 2)) + "min")    
        ax[1].legend(handles=patches, bbox_to_anchor=(1, -3.1), ncol=2)
        
        camera.snap()

    animation = camera.animate()
    animation.save('solidification_evolution_comparison.gif', 
                   writer = 'imagemagick')
        
def create_animation_solidification(cylinder, label):    
    temperatur_discret = np.linspace(cylinder.max(), cylinder.min(), 100)
    time_discret = get_time_discretization(temperatur_discret)
   
    fig = plt.figure()
    camera = Camera(fig)    
    patches = get_solid_liquid_patch()   
    
    for i, actual_temperature in enumerate(temperatur_discret):    
        plt.imshow(cylinder > actual_temperature, cmap='bwr')        
        plt.text(1, 5, "Temperature T = " + str(round(actual_temperature, 2)) +
                 "ºC")
        plt.text(1, 6, "Time t = " + str(round(time_discret[i], 2)) + "min")
        plt.legend(handles=patches, bbox_to_anchor=(1, -1.1), ncol=2)
        plt.title('Solidification evolution ' + label)
        camera.snap()
        
    animation = camera.animate()
    animation.save(label + '.gif', writer = 'imagemagick')

if __name__ == "__main__":
    #for key, bend_road in data.items():
    #    create_animation_solidification(bend_road, key)

    create_comparison_animation_solidification(data)