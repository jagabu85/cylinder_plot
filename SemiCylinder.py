from SemiCylinderSlice import SemiCylinderSlice
from ReferenceFrame import ReferenceFrame

class SemiCylinder():
    def __init__(self, point_0, point_1, slices_thickness,
                 radius, angle, start_angle, slices_discretization):
        """Constructor method
        discretization_points: contains the parameters to make the
        discretization of each chesse_slice_length.

        Note: The discreetization of the partial cylinder is defined by the
        size of the vector values.
        """
        self.values = None
        self.slices_thickness = slices_thickness
        self.radius = radius
        self.angle = angle
        self.angle_start = start_angle
        self.slices_discretization = slices_discretization
        self.start_point = point_0
        self.frame = ReferenceFrame(point_0, point_1)

        self.slices = []

    def create_slices(self):
        self.slices = []
        for i, com in enumerate(self.values):
            point_0 = self.start_point + \
                      i * self.slices_thickness * self.frame.n1

            self.slices.append(SemiCylinderSlice(point_0, self.frame,
                                                 self.slices_thickness,
                                                 self.radius, self.angle,
                                                 self.angle_start,
                                                 self.slices_discretization))

    def set_values(self, values):
        """
        The slices are create again each time the values is set to make sure
        the number of slices much the number of values to plot
        :param values: np.arrays with the values of each slices
        :return:
        """
        self.values = values
        self.create_slices()


    def plot(self, ax, m):
        for i, slice in enumerate(self.slices):
            surface_color = m.to_rgba(self.values[i])
            slice.plot(ax, surface_color)