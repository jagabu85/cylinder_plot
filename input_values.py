import numpy as np
import os 

###############################################################################
# Define input variables
###############################################################################
def get_title():
    return "Test"

def get_user_limits():
    """
    :return: Should be one of the two options:
    None -> Autocalculate limit used in the color bar plot
    [min, max] -> The min and max limits for the color bar plot
    """
    return [5, 5.2]
    #return None

def get_measuremnts():
    """
    Imput data, to be define for each test
    :return:
    """
    measurement_a = np.array(
        [5.15, 5.06, 5.15, 5.15, 5.19, 5.2, 5.18, 5.17, 5.21, 5.19, 5.14,
         5.18, 5.19, 5.17, 5.19, 5.17, 5.13, 5.18, 5.15, 5.15, 5.14, 5.16,
         5.16, 5.12, 5.16, 5.15, 5.16, 5.19, 5.14, 5.12, 5.12, 5.1, 5.08])
    measurement_b = np.array(
        [5.15, 5.02, 5.13, 5.14, 5.15, 5.16, 5.18, 5.17, 5.22, 5.16, 5.19,
         5.17, 5.16, 5.14, 5.17, 5.15, 5.15, 5.16, 5.13, 5.1, 5.14, 5.1,
         5.15, 5.13, 5.16, 5.13, 5.14, 5.16, 5.11, 5.14, 5.15, 5.12, 5.09])
    measurement_c = np.array(
        [5.12, 5.01, 5.08, 5.1, 5.14, 5.18, 5.14, 5.12, 5.18, 5.14, 5.15,
         5.12, 5.14, 5.11, 5.11, 5.09, 5.11, 5.11, 5.11, 5.11, 5.1, 5.09,
         5.12, 5.13, 5.16, 5.11, 5.18, 5.13, 5.14, 5.15, 5.11, 5.09, 5.08])
    measurement_d = np.array(
        [5.16, 5.02, 5.1, 5.13, 5.14, 5.15, 5.16, 5.16, 5.14, 5.18, 5.16,
         5.18, 5.12, 5.16, 5.13, 5.09, 5.12, 5.15, 5.15, 5.14, 5.13, 5.1,
         5.14, 5.11, 5.14, 5.14, 5.18, 5.14, 5.14, 5.14, 5.13, 5.11, 5.08])

    if not(len(measurement_a) == len(measurement_b) == len(measurement_c) == \
            len(measurement_d)):
        raise Exception("The number of elements in measurement_a, "
                        "measurement_b, measurement_c and measurement_d are "
                        "not the same. They should be equal, otherwhise can "
                        "not be ploted")

    return [measurement_a, measurement_b, measurement_c, measurement_d]

def get_path_to_save_plot():    
    path = 'C:\\Users\\mr7117\\OneDrive - HyperionMT\\HomeShare\\' \
           + 'Documents\\miscelanea\\plot_cylinder\\'
    
    if os.path.exists(path):
        return path
    else:
        return '.'

def get_a_quarter_info():
    """
    Imput data, to be define for each test
    :return:
    """
    quarter_info = {
        "angle_start" : 1 * np.pi,      # Angle for revolution partial cylinder
        "angle" :  0.5 * np.pi,         # angle of the partial cylinder
        "point_0" : np.array([0, 0, 0]),
        "point_1": np.array([1, 0, 0]),
        "slice_thickness": get_general_cylinder_info()["slice_thickness"],
        "radius": get_general_cylinder_info()["radius"],
        "slices_discretization": get_general_cylinder_info()["slices_discretization"]
    }

    return quarter_info

def get_b_quarter_info():
    """
    Imput data, to be define for each test
    :return:
    """
    quarter_info = {
        "angle_start" : 1.5 * np.pi,      # Angle for revolution partial
        # cylinder
        "angle" :  0.5 * np.pi,         # angle of the partial cylinder
        "point_0" : np.array([0, 0, 0]),
        "point_1": np.array([1, 0, 0]),
        "slice_thickness": get_general_cylinder_info()["slice_thickness"],
        "radius": get_general_cylinder_info()["radius"],
        "slices_discretization": get_general_cylinder_info()["slices_discretization"]
    }

    return quarter_info

def get_c_quarter_info():
    """
    Imput data, to be define for each test
    :return:
    """
    quarter_info = {
        "angle_start" : 1.5 * np.pi,      # Angle for revolution partial
        # cylinder
        "angle" :  0.5 * np.pi,         # angle of the partial cylinder
        "point_0" : np.array([0, 0.5, 0.55]),
        "point_1": np.array([1, 0.5, 0.55]),
        "slice_thickness": get_general_cylinder_info()["slice_thickness"],
        "radius": get_general_cylinder_info()["radius"],
        "slices_discretization": get_general_cylinder_info()["slices_discretization"]
    }

    return quarter_info

def get_d_quarter_info():
    """
    Imput data, to be define for each test
    :return:
    """
    quarter_info = {
        "angle_start" : 2 * np.pi,      # Angle for revolution partial cylinder
        "angle" :  0.5 * np.pi,         # angle of the partial cylinder
        "point_0" : np.array([0, 0.5, 0.55]),
        "point_1": np.array([1, 0.5, 0.55]),
        "slice_thickness": get_general_cylinder_info()["slice_thickness"],
        "radius": get_general_cylinder_info()["radius"],
        "slices_discretization": get_general_cylinder_info()["slices_discretization"]
    }

    return quarter_info

def get_general_cylinder_info():
    """
    Imput data, to be define for each test
    :return:
    """
    info = {
        "slice_thickness": 0.1,
        "radius": 0.5,
        "slices_discretization": {
            "length": 2,
            "radius": 2,
            "angle": 10
        }
    }

    return info